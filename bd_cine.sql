/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.27-log : Database - bd_cine
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bd_cine` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `bd_cine`;

/*Table structure for table `tab_genero` */

DROP TABLE IF EXISTS `tab_genero`;

CREATE TABLE `tab_genero` (
  `idGenero` varchar(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_genero` */

insert  into `tab_genero`(`idGenero`,`nombre`) values ('GEN001','Acción'),('GEN002','Animación'),('GEN003','Biografía'),('GEN004','Ciencia Ficción'),('GEN005','Comedia'),('GEN006','Drama'),('GEN007','Familiar'),('GEN008','Terror'),('GEN009','Thriller');

/*Table structure for table `tab_pelicula` */

DROP TABLE IF EXISTS `tab_pelicula`;

CREATE TABLE `tab_pelicula` (
  `idPelicula` varchar(10) NOT NULL,
  `idGenero` varchar(50) NOT NULL,
  `titulo_local` varchar(100) NOT NULL,
  PRIMARY KEY (`idPelicula`),
  KEY `idGenero_FK` (`idGenero`),
  CONSTRAINT `idGenero_FK` FOREIGN KEY (`idGenero`) REFERENCES `tab_genero` (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tab_pelicula` */

insert  into `tab_pelicula`(`idPelicula`,`idGenero`,`titulo_local`) values ('PEL001','GEN004','Animales fantásticos 2'),('PEL002','GEN008','Los hambrientos'),('PEL003','GEN008','La Sirena'),('PEL004','GEN008','Annabelle');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
